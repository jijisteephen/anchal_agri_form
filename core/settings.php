<?php

// Block Direct Access
defined('PJT_EXE') or die('Access Restricted ,now the website is offline.');

class settings {

    // property declaration
    /* declare some class properties and variables */

    //Site settings
    public $live_site = '';
    public $offline = '1'; //0-offline
    public $offline_message = 'Site is down for maintenance.<br /> Please check back again soon.';
    public $display_offline_message = '1';
    public $offline_image = '';
    public $error_report = '1'; //after developing set zero
    //DB Settings
    public $db = true; //its for connect db switch
    public $dbdrive = 'mysqli';
    public $host = 'localhost';
    public $user = 'root'; 
    public $password = ''; 
    public $database = 'anchal_farm_db'; 
    public $dbprefix = 'anchal_'; //DB TABLE PREFIX
    //SMTP  
    public $mailfrom = 'test@example.com';
    public $fromname = 'MY NAME';
    public $smtpauth = '0';
    public $smtpuser = '';
    public $smtppass = '';
    public $smtphost = 'smtp.example.com';
    public $smtpsecure = 'none';
    public $smtpport = '25';
    public $ftp_host = '127.0.0.1';
    public $ftp_port = '21';
    public $ftp_user = '';
    public $ftp_pass = '';
    public $ftp_root = '';
    public $ftp_enable = '0';

}
?>