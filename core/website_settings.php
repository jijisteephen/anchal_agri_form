<?php


// Block direct access
defined('PJT_EXE') or die('Access Restricted ,now the website is offline.');

class website_settings {

    // property declaration
    /* declare some class properties and variables */

    //SEO Settings
    public $MetaDesc = '';
    public $MetaKeys = '';
    public $MetaTitle = '1';
    public $MetaAuthor = '1';
    public $MetaVersion = '0';
    //Site settings
    public $live_site = 'http://beblec.co.in/';
    public $mail_url = 'http://beblec.co.in/'; //Mirrored url
    public $logo_path = 'images/logo.png';
    public $favicon_path = 'images/favicon.ico';
    public $site_name_title = 'beblec'; //Site Title
    //mail id declerations for forms
    //No reply mail
    public $noreply_mail = 'admin@beblec.co.in';
    public $noreply_mail_name = 'No Reply - beblec';
    //Contact Us
    public $contact_mail = 'test@webnink.com';
    public $contact_mail_name = 'Contact - beblec';
    
    public  $mail_career = "admin@beblec.co.in";
    
    public  $mail_enquiry = "admin@beblec.co.in";
    
    //Admin Mail
    public $admin_mail = '@gmail.com';
    public $admin_mail_name = 'Administirator - beblec';
    public $admin_status = "1"; //0-Offline
    public $admin_message = 'Your site is down for maintenance.<br /> Please check back again soon.';

}

?>