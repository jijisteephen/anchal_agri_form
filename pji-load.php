<?php
	/** PROJECT FUNCTION ACCESS ON **/
	define('PJT_EXE','project-name');
	define('PJI_COR_DIR','core/'); /**Project Core Directory */
	define('PJI_INC_DIR','includes/'); /**Front end Include Directory */
	define('PJI_DOC_DIR','documents/'); /**Uploaded Files,Images And Docs - Means Resume, Videos, pdf, doc... */
	define('PJI_UPL_DIR','upload/'); /**Image Upload folder - Means Product Images,... */
	define('PJI_IMG_DIR','images/'); /**Image Upload folder - Means Profile Photos,... */
	/*Admin Define Strats */
	define('PJI_HSR_DIR','home_slider/');
	define('PJI_PMV_DIR', 'promovideo/');
	define('PJI_ASR_DIR', 'about_slider/');
	define('PJI_ADM_DIR','admin/'); /**Admin PATH */
	define('PJI_STP_DIR','../'); /**Admin step to front end */
	require_once(PJI_COR_DIR."config.php");
	$db_pfx = "gpk_";
	require_once(PJI_COR_DIR."website_settings.php");
	
	$web= new website_settings();

	$site=$web->live_site;
	$mail_url=$web->mail_url;
	$logo=$site.$web->logo_path;
	$favicon=$site.$web->favicon_path;
	$title=$web->site_name_title;


	 //SEO Settings
	 $MetaDesc=$web->MetaDesc;
	 $MetaKeys=$web->MetaKeys;
	 $MetaTitle=$web->MetaTitle;
	 $MetaAuthor=$web->MetaAuthor;
	 $MetaVersion=$web->MetaVersion;
	 
	 $admin=$web->admin_status;
	 $admin_msg=$web->admin_message;
	 
	 //Mail IDS
	$mail_noreply=$web->noreply_mail;
	$mail_noreply_name=$web->noreply_mail_name;
	
	$mail_contact=$web->contact_mail;
	$mail_contact_name=$web->contact_mail_name;
	
	$mail_admin=$web->admin_mail;
	$mail_admin_name=$web->admin_mail_name;
        
        $mail_career=$web->mail_career;
        $mail_enquiry=$web->mail_enquiry;
	?>